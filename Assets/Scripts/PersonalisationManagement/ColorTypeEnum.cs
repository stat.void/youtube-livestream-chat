public enum ColorTypeEnum
{
    Primary,
    Secondary,
    ObjectBackground,
    Background,
    Highlight
}
